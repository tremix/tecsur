<?php

/**
 * @file
 * Override of Bootstrap page.tpl.php.
 */
?>
<header id="navbar" class="navbar navbar-default" role="banner">
  <div class="navbar-inner container">
     <div id="main-navs">
       <div class="containers">
        <div class="header-top"><?php print render($page['top']); ?></div>
        <div class="header">
          <?php if (!empty($logo)): ?>
          <div class="logo-inners"><div class="logo-inner">
            <a class="logo pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a></div></div>
          <?php endif; ?>
          <?php print render($page['header']); ?>
         </div>
         <div class="yamm clearfix">
          <?php if (!empty($page['yamm'])): ?>
            <div id="navyamm" class="nav-collapse menu-principal">
              <div class="inner-menu">
                <ul class="nav navbar-nav inner-menu-inner">
                  <?php print render($page['yamm']); ?>
                </ul>
              </div>
            </div>
          <?php endif; ?>
        </div>
    </div></div>
  </div>
</header>

<?php if (!empty($page['slider'])): ?>
  <div id="slideshow_container">
    <?php print render($page['slider']); ?>
  </div>
<?php endif; ?>

<div class="main-container container">

  <div class="row">

    <?php if (!empty($page['preface'])): ?>
      <div id="preface">
        <?php print render($page['preface']); ?>
      </div>  <!-- /#preface -->
    <?php endif; ?>  

    <section class="cm <?php print $content_width; ?>">  
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted hero-unit"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      
      <a id="main-content"></a>
      <?php if ($print_content): ?>
        <?php print render($title_prefix); ?>
        <?php if (!empty($title)): ?>
          <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
      <?php endif; ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <div class="well"><?php print render($page['help']); ?></div>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      
      <?php if (!empty($page['content_top'])): ?>
      <div id="page-top">
        <?php print render($page['content_top']); ?>
      </div>  <!-- /#sidebar-second -->
      <?php endif; ?>
      
      <?php if ($print_content): ?>
        <div id="content-inner">
          <?php print render($page['content']); ?>
        </div>
      <?php endif; ?>
      
      <?php if (!empty($page['content_bottom'])): ?>
      <div id="page-bottom">
        <?php print render($page['content_bottom']); ?>
      </div>  <!-- /#sidebar-second -->
      <?php endif; ?>
      
    </section>

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="s1 <?php print $sidebar_first_width; ?>" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>  

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="s2 <?php print $sidebar_second_width; ?>" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>
  </div>

    <?php if (!empty($page['postcript_top'])): ?>
      <div id="postcript_top" class="clearfix row">
        <div class="content">
        <?php print render($page['postcript_top']); ?>
        </div>
      </div>  <!-- /#preface -->
    <?php endif; ?>  

    <?php if (!empty($page['postcript_bottom'])): ?>
      <div id="postcript_bottom" class="clearfix row">
        <div class="content">
        <?php print render($page['postcript_bottom']); ?>
        </div>
      </div>  <!-- /#preface -->
    <?php endif; ?>  

</div>

<footer class="footer">
    <div id="footer-inner" class="container">
    <?php print render($page['footer']); ?>
    </div>
  </footer>
